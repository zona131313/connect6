package connect;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ConnectView {

    Socket cs;
    DataInputStream dis;
    DataOutputStream dos;

    public ConnectView(Socket cs){
        try {
            this.cs = cs;
            dis = new DataInputStream(cs.getInputStream());
            dos = new DataOutputStream(cs.getOutputStream());
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public byte getOp(){
        try{
            return dis.readByte();
        } catch (IOException e){
                e.printStackTrace();
                return -1;
        }
    }

    public void sendOp(byte op){
        try{
            dos.writeByte(op);
            dos.flush();
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public int getInt(){
        try{
            return dis.readInt();
        } catch (IOException e){
            e.printStackTrace();
            return -1;
        }
    }

    public void sendInt(int i){
        try{
            dos.writeInt(i);
            dos.flush();
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public void send2d(int x, int y){
        try{
            dos.writeInt(x);
            dos.writeInt(y);
            dos.flush();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public int[] get2d(){
        int[] res = new int[2];
        try {
            res[0] = dis.readInt();
            res[1] = dis.readInt();
        } catch (IOException e){
            e.printStackTrace();
            return null;
        }
        return res;
    }

    public boolean isConnect(){
        return cs.isConnected();
    }

    public void disconnect(){
        try {
            if(cs.isConnected()) {
                dis.close();
                dos.close();
                cs.close();
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

}
