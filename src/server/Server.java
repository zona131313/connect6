package server;

import game.Connect6;
import connect.ConnectView;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class Server {
    ServerConnectController player1;
    ServerConnectController player2;
    Connect6 game;

    private final static int PORT = 8080;
    private InetAddress ip = null;

    public Server(){

        ServerSocket serverSocket;
        Socket player1Socket;
        Socket player2Socket;

        try {
            ip = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }

        try {
            serverSocket = new ServerSocket(PORT, 0, ip );
            System.out.println("Server start");
            while(true) {
                player2Socket = serverSocket.accept();
                ConnectView view2 = new ConnectView(player2Socket);
                player2 = new ServerConnectController(this, view2, 2);
                player1Socket = serverSocket.accept();
                ConnectView view1 = new ConnectView(player1Socket);
                player1 = new ServerConnectController(this, view1, 1);

                startGame();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void startGame(){
        if(player1.start() && player2.start()){
            game = new Connect6();
            int[] move;
            move = player1.getMove();

            if(move != null && game.set(move[0], move[1]) == 0) {
                player2.sendMove(move[0], move[1]);
                while (game.getWinner() == 0) {
                    playerMove(player2, player1);
                    playerMove(player2, player1);
                    playerMove(player1, player2);
                    playerMove(player1, player2);
                }
            }
            else {
                player1.sendWinner(2);
                player2.sendWinner(2);
            }
        }
    }

    private void playerMove(ServerConnectController moveplayer, ServerConnectController waitplayer){
        if(game.getWinner() != 0){
            return;
        }
        int[] move = moveplayer.getMove();
        if(move == null){
            playerLose(moveplayer);
        }
        if(move.length == 1){
            if(game.addSize(move[0])){
                if(!waitplayer.sendResize(move[0])) {
                    playerLose(waitplayer);
                }
            }
            else{
                playerLose(moveplayer);
            }
            playerMove(moveplayer, waitplayer);
        }
        else {
            int winner = game.set(move[0], move[1]);
            if(winner != -1){
                if(waitplayer.sendMove(move[0], move[1])){
                    if(winner != 0){
                        playerLose(waitplayer);
                    }
                }
                else{
                    playerLose(waitplayer);
                }

            }
            else{
                playerLose(moveplayer);
            }
        }
    }

    private void playerLose(ServerConnectController player){
        if(player1 == player){
            player2.sendWinner(2);
            player1.sendWinner(2);
        }
        else{
            player1.sendWinner(1);
            player2.sendWinner(1);
        }
        player1.disconnect();
        player2.disconnect();
        System.exit(0);
    }
}
