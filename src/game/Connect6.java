package game;

public class Connect6 {
    private int size = 19;
    private int maxsize = 128;
    private int[][] desk;
    private int BLANK = 0;
    private int BLACK = 1;
    private int WHITE = 2;
    private int playermovecount = 0;
    private int winner = BLANK;
    private boolean isStart = false;
    private int playersmove = BLACK;

    private int whoWin(int x, int y){
        if(isOut(x, y)){
            return -1;
        }
        if(desk[x][y] == BLANK){
            return BLANK;
        }
        int count = 1;
        for (int i = x + 1, j = y + 1;  ((i < x + 6) && (j < y + 6)) && !isOut(i, j) && desk[i][j] == desk[x][y]; i++, j++, count++);
        for (int i = x - 1, j = y - 1;  ((i > x - 6) && (j > y - 6)) && !isOut(i, j) && desk[i][j] == desk[x][y]; i--, j--, count++);
        if(count >= 6){
            winner = desk[x][y];
            return winner;
        }
        count = 1;
        for (int i = x - 1, j = y + 1;  ((i > x - 6) && (j < y + 6)) && !isOut(i, j) && desk[i][j] == desk[x][y]; i--, j++, count++);
        for (int i = x + 1, j = y - 1;  ((i < x + 6) && (j > y - 6)) && !isOut(i, j) && desk[i][j] == desk[x][y]; i++, j--, count++);
        if(count >= 6){
            winner = desk[x][y];
            return winner;
        }
        count = 1;
        for (int i = x + 1;  (i < x + 6) && !isOut(i, y) && desk[i][y] == desk[x][y]; i++, count++);
        for (int i = x - 1;  (i > x - 6) && !isOut(i, y) && desk[i][y] == desk[x][y]; i--, count++);
        if(count >= 6){
            winner = desk[x][y];
            return winner;
        }
        count = 1;
        for (int i = y + 1;  (i < y + 6) && !isOut(x, i) && desk[x][i] == desk[x][y]; i++, count++);
        for (int i = y - 1;  (i > y - 6) && !isOut(x, i) && desk[x][i] == desk[x][y]; i--, count++);
        if(count >= 6){
            winner = desk[x][y];
            return winner;
        }
        if(isFull()){
            if(desk[x][y] == WHITE){
                winner = BLACK;
                return winner;
            }
            else {
                winner = WHITE;
                return winner;
            }
        }

        return BLANK;
    }

    private boolean isOut(int x, int y){
        if (x > size - 1 || y > size - 1 || x < 0 || y < 0){
            return true;
        }
        return false;
    }

    private boolean isFull(){
        for(int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                if (desk[i][j] == BLANK){
                    return false;
                }
            }
        }
        return true;
    }

    public Connect6(){
        desk = new int[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                desk[i][j] = BLANK;
            }
        }
    }

    public Connect6(int size, int maxsize){

        if(size >= 6){
            this.size = size;
        }

        if(size <= maxsize){
            this.maxsize = maxsize;
        } else {
            this.maxsize = size;
        }

        desk = new int[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                desk[i][j] = BLANK;
            }
        }
    }

    public int set(int x, int y){
        if(isOut(x, y) || desk[x][y] != BLANK || winner != BLANK){
            return -1;
        }
        if(isStart) {
            desk[x][y] = playersmove;

            playermovecount++;

            if (playermovecount == 2) {
                if (playersmove == BLACK) {
                    playersmove = WHITE;
                } else {
                    playersmove = BLACK;
                }
                playermovecount = 0;
            }

            return whoWin(x, y);
        } else {
            desk[x][y] = playersmove;
            playersmove = WHITE;
            isStart = true;
        }
        return BLANK;
    }

    public int get(int x, int y){
        if(isOut(x, y)){
            return -1;
        }
        return desk[x][y];
    }

    public int getPlayersmove(){
        return playersmove;
    }

    public boolean addSize(int how){
        if(isStart) {
            if (size + how > maxsize || how <= 0) {
                return false;
            }
            int newsize = size + 2 * how;
            int[][] newdesk = new int[newsize][newsize];

            for (int i = 0; i < newsize; i++) {
                for (int j = 0; j < newsize; j++) {
                    newdesk[i][j] = BLANK;
                }
            }

            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    newdesk[how + i][how + j] = desk[i][j];
                }
            }

            size = newsize;
            desk = newdesk;

            return true;
        }

        return false;
    }

    public int getWinner(){
        return winner;
    }

    public int getSize(){
        return size;
    }
}
