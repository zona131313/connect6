package server;

import connect.ConnectView;

public class ServerConnectController {

    // Операции:
    // -1 - ошибка/дисконнект,
    // 1 - неверно,
    // 2 - верно,
    // 3 - отправка/принятие выбранной координаты,
    // 4 - resize
    // 5 - отправка попедителя

    ConnectView view;
    Server server;
    Byte lastop;
    int player;

    public ServerConnectController(Server server, ConnectView view, int player){
        this.server = server;
        this.view = view;
        this.player = player;
    }

    public boolean start(){
        if(view.isConnect()) {
            System.out.println("starting");
            view.sendInt(player);
            lastop = 1;
            lastop = view.getOp();
            if (lastop == 2) {
                System.out.println("complete");
                return true;
            }
        }
        return false;
    }

    public void sendWinner(int winner){
        if(view.isConnect()) {
            System.out.println("sending winner");
            lastop = 5;
            view.sendOp(lastop);
            view.sendInt(winner);
            System.out.println("complete");
        }
    }

    public boolean sendResize(int size){
        if(view.isConnect()){
            System.out.println("send resize");
            lastop = 4;
            view.sendOp(lastop);
            view.sendInt(size);
            System.out.println("complete");
            return true;
        }
        return false;
    }

    public int[] getMove(){
        if(view.isConnect()) {
            System.out.println("getting move");
            lastop = 1;
            lastop = view.getOp();
            if(lastop == 1){
                return null;
            }
            if(lastop == 4){
                int[] resize = new int[1];
                resize[0] = view.getInt();
                return resize;
            }
            int[] move = view.get2d();
            System.out.println("complete");
            return move;
        }
        return null;
    }

    public boolean sendMove(int x, int y){
        if(view.isConnect()){
            System.out.println("sending move");
            lastop = 3;
            view.sendOp(lastop);
            view.send2d(x,y);
            System.out.println("complete");
            return true;
        }
        return false;
    }

    public void disconnect(){
        if(view.isConnect()) {
            System.out.println("disconnect");
            view.disconnect();
        }
    }
}
