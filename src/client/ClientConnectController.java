package client;

import connect.ConnectView;
import javafx.application.Platform;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;


public class ClientConnectController {
    // Операции:
    // -1 - ошибка/дисконнект,
    // 1 - неверно,
    // 2 - верно,
    // 3 - отправка/принятие выбранной координаты,
    // 4 - resize
    // 5 - отправка попедителя

    private ConnectView view;
    private final static int PORT = 8080;
    private InetAddress ip = null;
    private Byte lastop;
    private boolean isEnd = false;
    private boolean isErrorClose = false;
    private String errorMessage = "";
    private boolean resized = false;
    private int winner = -1;
    private ClientController controller;
    private int player;

    ClientConnectController(ClientController controller){
        this.controller = controller;
    }

    boolean start(){
        System.out.println("starting");
        try {

            ip = InetAddress.getLocalHost();

        } catch (UnknownHostException e){
            e.printStackTrace();
            controller.showError(e);
        }

        try {

            Socket socket = new Socket(ip, PORT);
            view = new ConnectView(socket);

        } catch (IOException e){
            e.printStackTrace();
            controller.showError(e);
        }

        if(view.isConnect()){
            (new Thread( new Runnable() {

                @Override
                public void run() {
                    System.out.println("new thread");
                    player = view.getInt();
                    view.sendOp((byte)2);

                    lastop = -2;

                    Runnable updater = new Runnable(){
                        @Override
                        public void run() {
                            if (isEnd) {
                                if (winner == player) {
                                    controller.win();
                                } else {
                                    controller.lose();
                                }
                            }
                            else if(resized){
                                controller.uiRedraw();
                                resized = false;
                            }
                            else if(isErrorClose){
                                controller.showError(errorMessage);
                            }
                            else{
                                controller.uiUpdate();
                            }
                        }
                    };

                    while (!isEnd && lastop != -1 && view.isConnect()) {
                        lastop = view.getOp();

                        if (lastop == 3) {
                            if(!enemyMove()){
                                view.disconnect();
                            }
                        }
                        if (lastop == 4) {
                            if(!resize()){
                                isErrorClose = true;
                                view.disconnect();
                                errorMessage = "resize exception";
                            }
                        }
                        if (lastop == 5) {
                            winner();
                        }
                        if (lastop == -1 || !view.isConnect()) {
                            if(view.isConnect()){
                                view.disconnect();
                            }
                            isErrorClose = true;
                            errorMessage ="disconnected";
                        }
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        Platform.runLater(updater);
                    }
                }
            })).start();
        }
        else {
            return false;
        }
        return true;
    }

    private boolean enemyMove(){
        System.out.println("getting move");
        int[] move = view.get2d();
        boolean res = controller.getMove(move[0], move[1]);
        if(res){
            System.out.println("complete");
        }
        return res;
    }

    void resize(int how){
        System.out.println("sending resize");
        view.sendOp((byte)4);
        view.sendInt(how);
        System.out.println("complete");
    }

    void setMove(int x, int y){
        System.out.println("sending move");
        view.sendOp((byte)3);
        view.send2d(x, y);
        System.out.println("complete");
    }

    int getPlayer() {
        return player;
    }

    private boolean resize(){
        System.out.println("getting resize");
        int how = view.getInt();
        if(controller.resize(how)){
            resized = true;
            System.out.println("complete");
            return true;
        }
        return false;
    }

    void disconnect(){
        System.out.println("disconnecting");
        view.sendOp((byte)-1);
        view.disconnect();
    }

    private void winner(){
        System.out.println("getting winner");
        winner = view.getInt();
        isEnd = true;
        System.out.println("complete");
    }

    public boolean isConnect(){
        return view.isConnect();
    }

}
