package client;

import game.Connect6;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.ArrayList;

public class ClientController {
    private Connect6 game;
    private ClientConnectController controller;
    private DeskDraw desk;
    private boolean ourMove = false;

    @FXML
    Slider zoomSlider;

    @FXML
    Button resizeButton;

    @FXML
    ChoiceBox<Integer> sizeBox;

    @FXML
    Label zoomLabel;

    @FXML
    Label moveLabel;

    @FXML
    Button connectButton;

    @FXML
    ScrollPane deskPane;

    @FXML
    AnchorPane mainPane;

    @FXML
    Group paintGroup;

    @FXML
    public void initialize(){
        resizeButton.setDisable(true);
        deskPane.setDisable(true);
        controller = new ClientConnectController(this);
        game = new Connect6();
        ArrayList<Integer> resize_choice = new ArrayList<Integer>();
        for (int i = 1; i < 9; i++){
            resize_choice.add(i);
        }
        sizeBox.setValue(1);
        sizeBox.setItems((ObservableList<Integer>) FXCollections.observableArrayList(resize_choice));
        zoomSlider.setMin(1.0d);
        zoomSlider.setMax(10.0d);
        zoomSlider.setValue(1.0d);
    }

    public void connectToServer(){
        if(controller.start()) {
            desk = new DeskDraw(paintGroup, game, deskPane);

            paintGroup.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(ourMove) {
                        int[] point = desk.getPoint(event.getX(), event.getY());
                        if(game.get(point[0], point[1]) == 0) {
                            controller.setMove(point[0], point[1]);
                            setMove(point[0], point[1]);
                            uiUpdate();
                        }
                    }
                }
            });

            deskPane.widthProperty().addListener((obs, oldVal, newVal) ->{
                desk.redraw(newVal.doubleValue(), deskPane.getHeight());
            });

            deskPane.heightProperty().addListener((obs, oldVal, newVal) ->{
                desk.redraw(deskPane.getWidth(), newVal.doubleValue());
            });

            mainPane.getScene().getWindow().setOnCloseRequest(event -> {
                controller.disconnect();
                System.exit(0);
            });

            zoomSlider.valueProperty().addListener((obs, oldVal, newVal) ->{
                desk.changeZoom(zoomSlider.getValue());
                zoomLabel.setText(String.format("Zoom: x%.2f", zoomSlider.getValue()));
            });

            ourMove = controller.getPlayer() == 1;

            if(ourMove){
                moveLabel.setText("yours move");
            }
            else {
                moveLabel.setText("enemy move");
            }

            connectButton.setDisable(true);
            resizeButton.setDisable(false);
            deskPane.setDisable(false);
        }
        else{
            showError("start exception");
        }
    }

    void uiUpdate(){
        desk.update();
        if(ourMove){
            moveLabel.setText("your's move");
        }
        else{
            moveLabel.setText("enemy move");
        }
    }

    void uiRedraw(){
        desk.redraw(-1, -1);
    }

    public void resize(){
        int how = sizeBox.getValue();
        if(ourMove && game.addSize(how)) {
            controller.resize(how);
            desk.redraw(-1, -1);
        }
    }

    boolean resize(int how){
        return game.addSize(how);
    }

    private void setMove(int x, int y){
        if(game.set(x, y) == 0) {
            ourMove = (game.getPlayersmove() == controller.getPlayer());
        }
    }

    boolean getMove(int x, int y){
        int res = game.set(x, y);

        if(res == -1) {
            return false;
        }

        ourMove = game.getPlayersmove() == controller.getPlayer();
        return true;
    }

    void lose(){
        ourMove = false;
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "You lose!", ButtonType.OK);
        alert.headerTextProperty().set(":(");
        alert.showAndWait();
    }

    void win(){
        ourMove = false;
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "You WIN!", ButtonType.OK);
        alert.headerTextProperty().set("Congratulations!");
        alert.showAndWait();
    }

    void showError(Exception e){

        Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.OK);
        alert.showAndWait();

        System.exit(1);
    }

    void showError(String e){
        Alert alert = new Alert(Alert.AlertType.ERROR, e, ButtonType.OK);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.OK) {
            System.exit(1);
        }
    }
}
