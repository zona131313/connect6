package client;

import game.Connect6;
import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

class DeskDraw {
    private Group desk_group;
    private double point_radius;
    private ScrollPane desk_pane;
    private Connect6 game;
    private double zoom;
    private ArrayList<Circle> circles;


    DeskDraw(Group desk_group, Connect6 game, ScrollPane desk_pane){
        this.desk_group = desk_group;
        this.desk_pane = desk_pane;
        this.game = game;
        zoom = 1.0d;
        redraw(-1, -1);
    }

    int[] getPoint(double x, double y){
        int[] point = new int[2];
        point[0] = (int)(x / (point_radius * 2));
        point[1] = (int)(y / (point_radius * 2));
        return point;
    }

    void redraw(double newwidth, double newheight){
        double w;
        double h;
        int recoil = 3;
        if(newwidth < 0){
            w = desk_pane.getWidth() - recoil;
        }
        else {
            w = newwidth - recoil;
        }
        if(newheight < 0){
            h = desk_pane.getHeight() - recoil;
        }
        else{
            h = newheight - recoil;
        }
        double min_size = (w > h)?h:w;
        point_radius = (((min_size) /game.getSize())/2) * zoom;
        desk_group.getChildren().clear();
        int R = 229;
        int G = 190;
        int B = 112;
        Rectangle rect = new Rectangle();
        rect.setFill(new Color(((double)R/255d),((double)G/255d),((double)B/255d),1.0));
        rect.setX(0);
        rect.setY(0);
        double height;
        double width;

        if((game.getSize() * point_radius * 2) > h) {
            height = game.getSize() * point_radius * 2;
        }
        else {
            height = h;
        }

        if((game.getSize() * point_radius * 2) > w){
            width = game.getSize() * point_radius * 2;
        }
        else{
            width = w;
        }
        rect.setHeight(height);
        rect.setWidth(width);
        desk_group.getChildren().add(rect);
        for (int i = 0; i < game.getSize(); i++){
            Line lineX = new Line();
            lineX.setStartX(0);
            lineX.setEndX(game.getSize() * point_radius * 2);
            lineX.setStartY((i * point_radius * 2) + point_radius);
            lineX.setEndY((i * point_radius * 2) + point_radius);
            lineX.setFill(Color.BLACK);
            Line lineY = new Line();
            lineY.setStartX((i * point_radius * 2) + point_radius);
            lineY.setEndX((i * point_radius * 2) + point_radius);
            lineY.setStartY(0);
            lineY.setEndY(game.getSize() * point_radius * 2);
            lineY.setFill(Color.BLACK);
            desk_group.getChildren().add(lineX);
            desk_group.getChildren().add(lineY);
        }

        circles = new ArrayList<>();
        addCircles();
    }

    void changeZoom(double zoom){
        this.zoom = zoom;
        redraw(-1, -1);
    }

    private void addCircles(){
        for (int i = 0; i < game.getSize(); i++){
            for (int j = 0; j < game.getSize(); j++){
                int player = game.get(i, j);
                if(player == 1 || player == 2) {
                    Circle circle = new Circle();
                    circle.setCenterX((i * point_radius * 2) + point_radius);
                    circle.setCenterY((j * point_radius * 2) + point_radius);
                    circle.setRadius(point_radius);
                    circle.setFill((player == 1)?(Color.BLACK):(Color.WHITE));
                    desk_group.getChildren().add(circle);
                    circles.add(circle);
                }
            }
        }
    }

    void update(){
        for(Circle circle: circles){
            desk_group.getChildren().remove(circle);
        }
        circles.clear();
        addCircles();
    }
}
